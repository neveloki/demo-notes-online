// @ is an alias to /src

export default {
  name: 'BrandButton',
  props: {
    title: {
      type: String,
      default: '',
    },
    icon: {
      type: String,
      default: '',
    },
    href: {
      default: null,
    },
    mods: {
      type: Object,
      default: () => ({}),
    },
    disabled: {
      type: Boolean,
      default: false,
    },
    type: {
      type: String,
      default: 'button',
    },
  },
  computed: {
    getMods() {
      return Object.entries(this.mods)
        .map(([key, value]) => `brand-button_${key}-${value}`);
    },
  },
};
