import NoteWidget from '../NoteWidget/NoteWidget.vue';

export default {
  name: 'Note',
  components: {
    NoteWidget,
  },
  data() {
    return {
      data: this.$store.state.widgets.filter((item) => item.id === +this.$route.params.id)[0] || null,
    };
  },
};
