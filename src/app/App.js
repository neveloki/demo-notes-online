import Header from '../components/blocks/Header/Header.vue';
import Footer from '../components/blocks/Footer/Footer.vue';

export default {
  name: 'App',
  components: {
    Header,
    Footer,
  },
  created() {
    this.$store.dispatch('getNoteList');
  },
};
