import Vue from 'vue';
import App from './app/App.vue';
import router from './router';
import store from './store';

Vue.config.productionTip = false;

const genUniqueKey = () => `${Math.floor(10000 * Math.random())}`;

Vue.mixin({
  methods: { genUniqueKey },
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
