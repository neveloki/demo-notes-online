import Vue from 'vue';
import Vuex from 'vuex';
import { Vue2Storage } from 'vue2-storage';

Vue.use(Vuex);
Vue.use(Vue2Storage, {
  prefix: 'note_',
  driver: 'local',
  ttl: 0, // вечно
});

export default new Vuex.Store({
  state: {
    widgets: [],
  },
  mutations: {
    save(state, payload) {
      // поиск объекта списка переданного на сохранение
      const index = state.widgets.findIndex((item) => item.id === payload.id);
      if (index >= 0) { // если объект заметки существует, обновляем свойства
        state.widgets[index].title = payload.title;
        state.widgets[index].items = payload.items;
      } else { // если нет, добавляем в список заметок
        state.widgets.push(payload);
      }
    },
    delete(state, id) {
      // поиск объекта списка переданного на удаление
      const index = state.widgets.findIndex((item) => item.id === id);
      if (index >= 0) { // если объект заметки существует, удаляем из списка
        state.widgets.splice(index, 1);
      }
    },
    saveList(state, payload) {
      state.widgets = payload;
    },
  },
  actions: {
    /*
    ** Обработчик загрузки сохраненных данных списка
     */
    getNoteList({ commit }) {
      try {
        const data = Vue.$storage.get('list');
        if (data) {
          commit('saveList', data);
        }
      } catch (err) {
        // console.warn(err);
      }
    },
    /*
    ** Обработчик сохранения данных списка
     */
    saveNote({ commit, state }, payload) {
      try {
        commit('save', payload);
        Vue.$storage.set('list', state.widgets);
      } catch (err) {
        // console.warn(err);
      }
    },
    /*
    ** Обработчик удаления заметки из списка
     */
    deleteNote({ commit, state }, id) {
      try {
        commit('delete', id);
        Vue.$storage.set('list', state.widgets);
      } catch (err) {
        // console.warn(err);
      }
    },
  },
  modules: {},
});
